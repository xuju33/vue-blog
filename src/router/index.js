import Vue from "vue";
import VueRouter from 'vue-router';



import home from '@/pages/home';
import about from '@/pages/about';
import article from '@/pages/article';
import apply from '@/pages/apply';
import ArticleEdit from '@/pages/ArticleEdit';
import MyArticle from '@/pages/MyArticle';
import Login from "@/components/Login";
import Register from "@/components/Register";

import archive from '@/pages/archive';
Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    component: home,
  },
  {
    path: '/home',
    name: '首页',
    component: home,
  },
  {
    path: '/about',
    name: '关于',
    component: about,
  },

  {
    path: '/tag/:name',
    name: 'tag',
    component: archive,
  },
  {
    path: '/MyArticle',
    name: '我的文章',
    component: MyArticle,
  },
  {
    path: '/ArticleEdit/:id',
    name: '新增文章',
    component: ArticleEdit,
  },
  {
    path: '/article/:id',
    name: '文章详情',
    component: article,
  },
  {
    path: '/apply',
    name: 'apply',
    component: apply,
  },
  {
    path: '/archive',
    name: 'archive',
    component: archive,
  },
  {
    path: "/login",
    name: "登录",
    component: Login
  },
  {
    path: "/register",
    name: "注册",
    component: Register
  },
  {
    path: '*',
    redirect: '/',
  },
];

const router = new VueRouter({
  routes,
  mode: 'history',
});

router.beforeEach((to, from, next) => {
  // 登录界面登录成功之后，会把用户信息保存在会话
  // 存在时间为会话生命周期，页面关闭即失效。
  console.log("ROUTE WITH token" )
  let isLogin = sessionStorage.getItem("TOKEN");
  // let roles = sessionStorage.getItem("ROLES");
  // let permissions = sessionStorage.getItem("PERMISSIONS");
  // console.log("permission-----: " + permissions);
  // 未登录
  if (!isLogin) {
    if (to.path === "/login" || to.path === "/register") next();
    else next({ path: "/login" });
  } else {
    // 已登录
    //去的路由为登录页，重定向至首页
    if (to.path === "/login") next({ path: "/" });
    else {
      next();
    }
  }
});

export default router;
