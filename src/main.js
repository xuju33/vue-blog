import Vue from 'vue';
import App from './App.vue';
import ElementUI from 'element-ui';
import router from './router';
import 'element-ui/lib/theme-chalk/index.css';
import 'element-ui/lib/theme-chalk/display.css';
import './assets/style.css';
import fHeader from './components/f-header';
import fFooter from './components/f-footer';
import i18n from './i18n/i18n';
import api from './http';
import mavonEditor from "mavon-editor";
import "mavon-editor/dist/css/index.css";
import store from "./store";
// use
Vue.config.productionTip = false;
Vue.use(ElementUI);
Vue.use(mavonEditor); 

Vue.use(api);
Vue.component('f-header', fHeader);
Vue.component('f-footer', fFooter);

//Vue.use(ElementUI, {
//i18n: (key, value) => i18n.t(key, value)
//})


new Vue({
  router,
  i18n,
  store,
  render: (h) => h(App),
}).$mount('#app');
