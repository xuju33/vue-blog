const user = {
  state: {
    username: "",
    token: "",
    tokenFlag: "false"
  },
  mutations: {
    SET_USERNAME: (state, username) => {
      state.username = username;
    },
    SET_TOKEN: (state, token) => {
      state.token = token;
    },
    SET_TOKENFLAG: (state, flag) => {
      state.tokenFlag = flag;
    }
  }
};
export default user;
