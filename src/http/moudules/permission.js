import axios from "../axios";
//全部
export const getAllList = () => {
  return axios({
    url: "/roles/allPermissions",
    method: "get"
  });
};
