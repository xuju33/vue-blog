import axios from '../axios';
/*
 * 博客管理模块
 */

export const getList = (params) => {
  return axios({
    url: '/blogs',
    method: 'get',
    params,
  });
};

//获取某个用户的所有博客
export const getMyList = (params) => {
  return axios({
    url: '/blogs/own',
    method: 'get',
    params,
  });
};

export const get = (id) => {
  // return axios.get(`/user/${id}`);
  const tmp = id.id
    return axios({
    url: `/blogs/${tmp}`,
    method: "get"
  });
};

export const edit = (data) => {
  return axios({
    url: '/blogs',
    method: 'put',
    data,
  });
};

export const insert = (data) => {
  return axios({
    url: '/blogs',
    method: 'post',
    data,
  });
};

export const increseReads = (id) => {
  const tmp = id.id
  return axios({
    url: `/blogs/read/${tmp}`,
    method: 'put',
  });
};

export const increseLikes = (id) => {
  const tmp = id.id
  return axios({
    url: `/blogs/like/${tmp}`,
    method: 'put',
  });
};
// export const delete = id => {
//   return axios({
//     url: `/blogs/${global.deleteId}`,
//     method: "delete"
//   });
// };
