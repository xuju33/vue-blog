import axios from '../axios';
/*
 * 角色管理模块
 */

//全部角色
export const getAllList = () => {
  return axios({
    url: '/roles/allRoles',
    method: 'get',
  });
};

//分页
export const getList = (params) => {
  return axios({
    url: '/roles',
    method: 'get',
    params,
  });
};

export const get = (id) => {
  return axios.get(`/roles/${id}`);
};

export const edit = (data) => {
  return axios({
    url: '/roles',
    method: 'put',
    data,
  });
};
export const insert = (data) => {
  return axios({
    url: '/roles',
    method: 'post',
    data,
  });
};
// export const delete = id => {
//   return axios({
//     url: `/users/${global.deleteId}`,
//     method: "delete"
//   });
// };
