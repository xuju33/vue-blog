import axios from '../axios';
/*
 * 用户管理模块
 */

export const getList = (params) => {
  return axios({
    url: '/users',
    method: 'get',
    params,
  });
};

export const getNameList = (data) => {
  return axios({
    url: '/users/name',
    method: 'post',
    data,
  });
};

export const get = (id) => {
  return axios.get(`/user/${id}`);
};
export const edit = (data) => {
  return axios({
    url: '/users',
    method: 'put',
    data,
  });
};
export const insert = (data) => {
  return axios({
    url: '/users',
    method: 'post',
    data,
  });
};
// export const delete = id => {
//   return axios({
//     url: `/users/${global.deleteId}`,
//     method: "delete"
//   });
// };
