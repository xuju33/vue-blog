/* 
 * 接口统一集成模块
 */
import * as login from "./moudules/login";
import * as user from "./moudules/user";
import * as blog from "./moudules/blog";
import * as role from "./moudules/role";
import * as permission from "./moudules/permission";
// import * as menu from './moudules/menu'

// 默认全部导出
export default {
  login,
  user,
  blog,
  role,
  permission
  // menu
};
